package bot

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/lod/botespecialista/commands"
	"gitlab.com/lod/botespecialista/config"
)

// BotID is the ID returned by Discord
var BotID string
var discordSession *discordgo.Session

// Start initializes de bot
func Start() {
	discordSession, err := discordgo.New("Bot " + config.Token)
	if err != nil {
		fmt.Println("Error:", err.Error())
		return
	}

	u, err := discordSession.User("@me")
	if err != nil {
		fmt.Println("Error:", err.Error())
		return
	}

	BotID = u.ID
	discordSession.GuildMemberNickname(config.GuildID, "@me", config.Nick)
	discordSession.AddHandler(messageHandler)

	err = discordSession.Open()
	if err != nil {
		fmt.Println("Error:", err.Error())
		return
	}

	fmt.Println("Bot is running")
}

func messageHandler(session *discordgo.Session, msg *discordgo.MessageCreate) {
	if strings.HasPrefix(msg.Content, config.Prefix) {
		if msg.Author.ID == BotID {
			return
		}

		dcs := commands.DiscordSession{
			Msg:     msg,
			Session: session,
		}

		message := strings.TrimPrefix(msg.Content, config.Prefix)
		messageValues := strings.Split(message, " ")
		command := messageValues[:1][0]
		args := messageValues[1:]

		handler := commands.Handler{
			Command: command,
			Args:    args,
		}

		handlers(handler, dcs)
	}
}
