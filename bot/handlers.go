package bot

import (
	"gitlab.com/lod/botespecialista/commands"
)

func handlers(h commands.Handler, dcs commands.DiscordSession) {
	h.Handle("register", dcs, commands.Register)
	h.Handle("list", dcs, commands.List)
	h.Handle("user", dcs, commands.User)
	h.Handle("me", dcs, commands.Me)
	h.Handle("call", dcs, commands.Call)
	h.Handle("add", dcs, commands.Add)
	h.Handle("rm", dcs, commands.Rm)
	h.Handle("help", dcs, commands.Help)
}
