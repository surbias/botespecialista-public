package models

import (
	"strings"
)

// Specialty model type struct
type Specialty struct {
	ID    int    `gorm:"id"`
	Name  string `gorm:"name"`
	Slug  string `gorm:"slug"`
	Image string `gorm:"image"`
	Color string `gorm:"color"`
	Users []User `gorm:"many2many:user_specialties;"`
}

// GetSpecialty returns a specility by its name
func GetSpecialty(slug string) (spec Specialty) {
	db.Where("slug = ?", strings.ToLower(slug)).First(&spec)
	return spec
}

// GetUsers returns all users from a specialty
func (specialty Specialty) GetUsers() (users []User) {
	users = []User{}
	db.Model(&specialty).Related(&users, "Users")

	return users
}

// GetSpecialties returns all specilities
func GetSpecialties() (specs []Specialty) {
	db.Order("name ASC").Find(&specs)
	return specs
}

// GetListSpecialties returns a string list of specialties
func GetListSpecialties(specialties []Specialty) (list string) {
	for _, spec := range specialties {
		list += spec.Name + ", "
	}

	list = strings.TrimSuffix(list, ", ")

	if list == "" {
		list = "Sem especialidades"
	}

	return list
}
