package models

import (
	"github.com/jinzhu/gorm"
	// this is used to load postgres
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var db *gorm.DB

// Connect connects to the database
func Connect() {
	db, _ = gorm.Open("postgres", "host=xxxx user=xxxx dbname=xxxx sslmode=require password=xxxx")
}
