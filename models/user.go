package models

import (
	"strings"
)

// User model type struct
type User struct {
	ID          int         `gorm:"id"`
	Name        string      `gorm:"name"`
	Username    string      `gorm:"username"`
	DiscordID   string      `gorm:"discord_id"`
	Specialties []Specialty `gorm:"many2many:user_specialties;"`
}

// GetUser returns a user by its discord_id
func GetUser(discordID string) (user User) {
	db.Where("discord_id = ?", discordID).First(&user)
	return user
}

// CheckUserExistence returns true if the user with this id already exists
func CheckUserExistence(discordID string) bool {
	user := User{}
	db.Where("discord_id = ?", discordID).First(&user)

	if user.ID == 0 {
		return false
	}

	return true
}

// GetSpecialties returns all specialties from a user
func (user User) GetSpecialties() (specs []Specialty) {
	specialties := []Specialty{}
	db.Model(&user).Order("slug ASC").Related(&specialties, "specialties")

	return specialties
}

// NewUser creates and returns a new user
func NewUser(name string, username string, discordID string) (user User) {
	user = User{
		Name:      name,
		Username:  username,
		DiscordID: discordID,
	}
	db.NewRecord(user)
	db.Create(&user)
	return user
}

// GetListUsers returns a string list of users
func GetListUsers(users []User) (list string) {
	for _, user := range users {
		list += user.Username + ", "
	}

	list = strings.TrimSuffix(list, ", ")

	if list == "" {
		list = "Sem utilizadores"
	}

	return list
}

// GetUserByDiscordID returns a user by its discord id
func GetUserByDiscordID(id string) (user User) {
	db.Where("discord_id = ?", id).First(&user)
	return user
}

// AddSpecialty relates a user to a specialty
func (user User) AddSpecialty(specialty Specialty) {
	if specialty.ID != 0 {
		db.Model(&user).Association("specialties").Append(specialty)
	}
}

// RemoveSpecialty relates a user to a specialty
func (user User) RemoveSpecialty(specialty Specialty) {
	db.Model(&user).Association("specialties").Delete(&specialty)
}

// UpdateUser updates the data of a user
func UpdateUser(name string, username string, discordID string) {
	user := User{}
	db.Where("discord_id = ?", discordID).First(&user)
	user.Name = name
	user.Username = username
	db.Save(user)
}
