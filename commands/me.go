package commands

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/lod/botespecialista/config"
	"gitlab.com/lod/botespecialista/models"
)

// Me shows a list of the user specialties
func Me(dcs DiscordSession, args []string) {
	discordID := dcs.Msg.Author.ID

	user := models.GetUserByDiscordID(discordID)
	if user.ID != 0 {
		specialties := user.GetSpecialties()
		embed := discordgo.MessageEmbed{
			Title:       "As minhas especialidades",
			Description: models.GetListSpecialties(specialties),
			Color:       config.Color,
		}

		_, _ = dcs.Session.ChannelMessageSendEmbed(dcs.Msg.ChannelID, &embed)
	} else {
		_, _ = dcs.Session.ChannelMessageSend(dcs.Msg.ChannelID, "Este utilizador não existe na base de dados.")
	}
}
