package commands

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/lod/botespecialista/config"
)

// Help shows a list of comands and its info
func Help(dcs DiscordSession, args []string) {
	embed := discordgo.MessageEmbed{
		Title:       "Ajuda!",
		Description: "Olá eu sou o botespecialista, eu sei, nome pouco original. Para vos ajudar a utilizar os meus poderes, deixo em baixo uma lista de comandos e a sua descrição. Se tiveres dificuldade contacta um moderador.",
		Color:       config.Color,
		Fields: []*discordgo.MessageEmbedField{
			{
				Name:   "!help",
				Value:  "É este mesmo comando, acabaste de o executar.",
				Inline: false,
			},
			{
				Name:   "!e register",
				Value:  "Regista o utilizador na base de dados, se o utilizador já estiver registado, o comando actualiza os dados.",
				Inline: false,
			},
			{
				Name:   "!e list",
				Value:  "Mostra uma lista das especialidades existentes que podes usar.",
				Inline: false,
			},
			{
				Name:   "!e list <especialiade>",
				Value:  "Mostra uma lista, sem fazer mention, de todos os utilizadores que têm a especialidade desejada.",
				Inline: false,
			},
			{
				Name:   "!e add <especialidade> [especialidade...]",
				Value:  "Adiciona o utilizador que requisitou o comando à especialidade ou especialidades desejadas. É obrigatório definir pelo menos uma, mas podes separar por espaços para adicionares várias de uma vez só.",
				Inline: false,
			},
			{
				Name:   "!e rm <especialidade> [especialidade...]",
				Value:  "Remove o utilizador que requisitou o comando das especialidade ou especialidades desejadas. É obrigatório definir pelo menos uma, mas podes separar por espaços para removeres várias de uma vez só",
				Inline: false,
			},
			{
				Name:   "!e call <especialidade>",
				Value:  "Faz um mention a todos os utilizadores que tenham a especialidade desejada para pedir ajuda.",
				Inline: false,
			},
			{
				Name:   "!e me",
				Value:  "Mostra as suas próprias especialidades.",
				Inline: false,
			},
			{
				Name:   "!e user [username]",
				Value:  "Mostra as especialidades de um utilizador desejado, ou de si próprio caso não se selecione nenhum utilizador.",
				Inline: false,
			},
		},
	}
	_, _ = dcs.Session.ChannelMessageSendEmbed(dcs.Msg.ChannelID, &embed)
}
