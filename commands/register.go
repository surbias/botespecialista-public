package commands

import (
	"gitlab.com/lod/botespecialista/models"
)

// Register a new user on the database
func Register(dcs DiscordSession, args []string) {
	name := dcs.Msg.Author.Username + "#" + dcs.Msg.Author.Discriminator

	userExists := models.CheckUserExistence(dcs.Msg.Author.ID)
	if userExists {
		models.UpdateUser(name, dcs.Msg.Author.Username, dcs.Msg.Author.ID)
		_, _ = dcs.Session.ChannelMessageSend(dcs.Msg.ChannelID, "Dados alterados com sucesso!")
	} else {
		_ = models.NewUser(name, dcs.Msg.Author.Username, dcs.Msg.Author.ID)
		_, _ = dcs.Session.ChannelMessageSend(dcs.Msg.ChannelID, "Utilizador registado com sucesso!")
	}
}
