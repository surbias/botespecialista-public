package commands

import (
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/lod/botespecialista/config"
	"gitlab.com/lod/botespecialista/models"
)

// User shows a list of a user specialties
func User(dcs DiscordSession, args []string) {
	id := ""
	if len(args) < 1 {
		id = dcs.Msg.Author.ID
	} else if len(args) == 1 {
		if strings.HasPrefix(args[0], "<@!") {
			id = strings.TrimSuffix(strings.TrimPrefix(args[0], "<@!"), ">")
		} else {
			id = strings.TrimSuffix(strings.TrimPrefix(args[0], "<@"), ">")
		}
	}

	user := models.GetUser(id)

	if user.ID != 0 {
		specialties := user.GetSpecialties()
		embed := discordgo.MessageEmbed{
			Title:       user.Username,
			Description: models.GetListSpecialties(specialties),
			Color:       config.Color,
		}

		_, _ = dcs.Session.ChannelMessageSendEmbed(dcs.Msg.ChannelID, &embed)
	} else {
		_, _ = dcs.Session.ChannelMessageSend(dcs.Msg.ChannelID, "Este utilizador não existe na base de dados.")
	}
}
