package commands

import (
	"gitlab.com/lod/botespecialista/models"
)

// Add specialties to a user
func Add(dcs DiscordSession, args []string) {
	if len(args) > 0 {
		userExists := models.CheckUserExistence(dcs.Msg.Author.ID)

		if userExists {
			user := models.GetUserByDiscordID(dcs.Msg.Author.ID)
			inexistentSpecialties := false

			for _, specialtyName := range args {
				specialty := models.GetSpecialty(specialtyName)
				if specialty.ID != 0 {
					user.AddSpecialty(specialty)
				} else {
					inexistentSpecialties = true
				}
			}

			if inexistentSpecialties {
				_, _ = dcs.Session.ChannelMessageSend(dcs.Msg.ChannelID, "Uma ou várias especialidades não existentes. Use o comando `!e list` para ver as especialidades disponíveis.")
			} else {
				if len(args) == 1 {
					_, _ = dcs.Session.ChannelMessageSend(dcs.Msg.ChannelID, "Especialidade adicionada com sucesso!")
				} else {
					_, _ = dcs.Session.ChannelMessageSend(dcs.Msg.ChannelID, "Especialidades adicionadas com sucesso!")
				}
			}
		} else {
			_, _ = dcs.Session.ChannelMessageSend(dcs.Msg.ChannelID, "Ainda não estás registado na base de dados. `!e help` para mais informações.")
		}
	}
}
