package commands

import (
	"gitlab.com/lod/botespecialista/models"
)

// Call mentions all users of a certain specialty
func Call(dcs DiscordSession, args []string) {
	if len(args) > 0 {
		specName := args[0]
		specialty := models.GetSpecialty(specName)
		if specialty.ID != 0 {
			users := specialty.GetUsers()

			if len(users) > 0 {
				mentions := ""
				for _, user := range users {
					if user.DiscordID != dcs.Msg.Author.ID {
						mentions += "<@" + user.DiscordID + "> "
					}
				}

				_, _ = dcs.Session.ChannelMessageSend(dcs.Msg.ChannelID, mentions)
				_, _ = dcs.Session.ChannelMessageSend(dcs.Msg.ChannelID, "O "+dcs.Msg.Author.Username+" tem uma questão sobre "+specName)
			} else {
				_, _ = dcs.Session.ChannelMessageSend(dcs.Msg.ChannelID, "Esta especialidade não tem nenhum utilizador associado. Safa-te...")
			}
		} else {
			_, _ = dcs.Session.ChannelMessageSend(dcs.Msg.ChannelID, "Esta especialidade não existe na base de dados.")
		}
	} else {
		_, _ = dcs.Session.ChannelMessageSend(dcs.Msg.ChannelID, "Tem ser selecionada uma especialidade.")
	}
}
