package commands

import "github.com/bwmarrin/discordgo"

type commandFunc func(DiscordSession, []string)

// Handler é o tipo da mensagem recebida
type Handler struct {
	Command string
	Args    []string
}

// DiscordSession é para faciliar
type DiscordSession struct {
	Msg     *discordgo.MessageCreate
	Session *discordgo.Session
}

// Handle reenchaminha para o sitio correcto
func (h Handler) Handle(command string, dcs DiscordSession, fn commandFunc) {
	if string(h.Command) == command {
		fn(dcs, h.Args)
	}

	return
}
