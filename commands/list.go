package commands

import (
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/lod/botespecialista/config"
	"gitlab.com/lod/botespecialista/models"
)

// List shows a list of specialties
func List(dcs DiscordSession, args []string) {
	if len(args) < 1 {
		listSpecialties(dcs)
	} else if len(args) == 1 {
		specName := args[0]
		listUsers(dcs, specName)
	}
}

func listSpecialties(dcs DiscordSession) {
	specialties := models.GetSpecialties()
	embed := discordgo.MessageEmbed{
		Title:       "Especialidades Disponíveis",
		Description: models.GetListSpecialties(specialties),
		Color:       config.Color,
	}

	_, _ = dcs.Session.ChannelMessageSendEmbed(dcs.Msg.ChannelID, &embed)
}

func listUsers(dcs DiscordSession, specName string) {
	specialty := models.GetSpecialty(specName)

	if specialty.ID != 0 {
		users := specialty.GetUsers()
		color, _ := strconv.ParseInt(strings.TrimPrefix(specialty.Color, "#"), 16, 64)
		embed := discordgo.MessageEmbed{
			Title:       "Utilizadores de " + specialty.Name,
			Description: models.GetListUsers(users),
			Color:       int(color),
			Thumbnail: &discordgo.MessageEmbedThumbnail{
				URL:      specialty.Image,
				ProxyURL: "",
				Width:    64,
				Height:   64,
			},
		}

		_, _ = dcs.Session.ChannelMessageSendEmbed(dcs.Msg.ChannelID, &embed)
	} else {
		_, _ = dcs.Session.ChannelMessageSend(dcs.Msg.ChannelID, "Esta especialidade não existe na base de dados.")
	}
}
