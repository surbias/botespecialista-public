package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

var (
	// Token is the auth token from discord
	Token string
	// Prefix is the prefix of the bot commands
	Prefix string
	// GuildID is the server ID
	GuildID string
	// Nick is the bot's nickname
	Nick string
	// Color is the color of the left stripe for embed contents
	Color int

	configReader *configFile
)

type configFile struct {
	Token   string `json:"token"`
	Prefix  string `json:"prefix"`
	GuildID string `json:"guild-id"`
	Nick    string `json:"nickname"`
	Color   int    `json:"color"`
}

// ReadConfig goes to the configuration file and reads back the info
func ReadConfig() error {
	fmt.Println("Reading from config file...")

	file, err := ioutil.ReadFile("./config.json")
	if err != nil {
		fmt.Println("Error:", err.Error())
		return err
	}

	err = json.Unmarshal(file, &configReader)
	if err != nil {
		fmt.Println("Error:", err.Error())
		return err
	}

	Token = configReader.Token
	Prefix = configReader.Prefix
	GuildID = configReader.GuildID
	Nick = configReader.Nick
	Color = configReader.Color

	return nil
}
