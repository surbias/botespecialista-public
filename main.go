package main

import (
	"fmt"

	"gitlab.com/lod/botespecialista/bot"
	"gitlab.com/lod/botespecialista/config"
	"gitlab.com/lod/botespecialista/models"
)

func main() {
	err := config.ReadConfig()
	if err != nil {
		fmt.Println("Error:", err.Error())
		return
	}

	models.Connect()
	bot.Start()

	<-make(chan struct{})

	return
}
